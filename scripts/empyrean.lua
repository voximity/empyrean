-- empyrean -----------------------------------------------------------
--   by voximity                                                     --
--                                                                   --
-- empyrean is an experimental in-game console designed for elysian. --
-- it is modular and extensive, and allows for creating mini-cheats  --
-- in-game, such as aimbot.                                          --
-----------------------------------------------------------------------


-- USER PREFERENCE ----------------------------------------------------

DEBUG_MODE = 3 -- 0 for none, 1 for elysian console, 2 for empyrean console, 3 for all
CONSOLE_KEY = Enum.KeyCode.BackSlash
MODULE_FILES = {

	-- module names are snake_case
	-- store in workspace/empyrean/module_name.lua
	-- file names w/o .lua required
	-- no directory path, must stay at root empyrean directory
	-- order in this array as necessary

	"default_ui",
	"modules",
	"man",
	"keyboard",
	"aim",
	"movement"


}

-----------------------------------------------------------------------

local rprint = print
local rerror = error
local print = printconsole
local error = function(x) printconsole(x, 255, 0, 0 ) error(x) end

E = {}

E.debug = function(text)
	if DEBUG_MODE == 0 then return
	elseif DEBUG_MODE == 1 then printconsole(text)
	elseif DEBUG_MODE == 2 then E.out(text)
	elseif DEBUG_MODE == 3 then printconsole(text) E.out(text)
	end
end
E.output = {}
E.out = function(text, color)
	table.insert(E.output, {text = text, color = color})
end

E.modules = {}
E.getCommands = function()
	local commands = {}
	for i,v in next, E.modules do
		for a,b in next, v.commands do
			table.insert(commands, b)
		end
	end
	return commands
end

E.hardSplit = function(raw, div)
	local words = {}
	local pos = 1
	local remaining = raw
	
	local i = 1
	repeat
		local token = raw:sub(i, i + #div - 1)
		if token == div then
			table.insert(words, raw:sub(pos, i - 1))
			i = i + #div
			pos = i
			remaining = raw:sub(pos)
		end
		i = i + 1
	until i > #raw

	table.insert(words, remaining)

	return words
end
E.split = function(raw, div)
	local divider = div or " "
	if raw == "" or type(raw) ~= "string" then return {""} end
	local pos, words, str, lock = 0, {}, false, false

	for i = 1, #raw do
		if not lock then
			local char = raw:sub(i, i)
			if char == "\"" then
				if not str then
					pos = i + 1
					str = true
				elseif str then
					table.insert(words, string.sub(raw, pos, i - 1))
					pos = i + 2
					str = false
					lock = true
				end
			elseif char == divider and not str then
				table.insert(words, string.sub(raw, pos, i - 1))
				pos = i + 1
			end
		else
			lock = false
		end
	end

	if #(raw:sub(pos)) ~= 0 then
		table.insert(words, string.sub(raw, pos))
	end

	return words
end
E.parseCommand = function(raw)
	if raw == "" or raw == " " then return end

	local cmds = E.hardSplit(raw, "&&")
	if #cmds > 1 then
		for i,v in next, cmds do
			E.parseCommand(v)
		end
	end

	local args = E.split(raw)
	local command = args[1]
	local foundcommand = -1
	for i,v in next, E.getCommands() do
		for a,b in next, v.aliases do
			if b:lower() == command:lower() then
				foundcommand = v
			end
		end
	end
	if foundcommand ~= -1 then
		local otherargs = {}
		local rawargs = ""
		for i,v in next, args do
			if i ~= 1 then
				table.insert(otherargs, v)
				rawargs = rawargs .. v .. " "
			end
		end
		if rawargs:sub(#rawargs, #rawargs) == " " then
			rawargs = rawargs:sub(1, #rawargs - 1)
		end
		local success, error = pcall(function()
			foundcommand.func(otherargs, rawargs)
		end)
		if not success then
			E.out("failed to run command: " .. tostring(error), Color3.new(1, 0, 0))
		end
	end
end

E.players = {}
E.players.match = function(raw)
	return E.players.matchSome(raw)[1]
end
E.players.matchSome = function(raw)
	local targets = {}
	local players = game:GetService("Players"):GetPlayers()
	local function f(p) table.insert(targets, p) end
	local me = game:GetService("Players").LocalPlayer
	if raw == "me" or raw == "myself" then
		f(me)
	elseif raw == "all" then
		for i,v in next, players do
			f(v)
		end
	elseif raw == "others" then
		for i,v in next, players do
			if v ~= me then
				f(v)
			end
		end
	else
		for i,v in next, players do
			if v.Name:lower():sub(1, #raw) == raw:lower() then
				f(v)
			end
		end
	end
	return targets
end

E.createCommand = function(aliases, name, description, func)
	local command = {aliases = aliases, name = name, description = description, func = func}
	return command
end
E.createModule = function(file, name, description)
	local module = {}

	module.commands = {}

	module.file = file
	module.name = name
	module.description = description

	module.unload = function() end
	module.init = function() end

	module.bindCommand = function(command) table.insert(module.commands, command) end
	return module
end

E.bindModule = function(module)
	table.insert(E.modules, module)
	module.init()
end

local loaded = {}

E.loadModule = function(v)
	if v:find("/") or v:find("\\") then
		error("A module file definition declares a unique path: " .. v)
	end

	for a,b in next, loaded do if v == b then error("A module file has already been loaded: " .. v) end end

	local path = "empyrean\\" .. v .. ".lua"

	local success, error = pcall(function()
		loadstring(readfile(path))()
		table.insert(loaded, v)
		E.debug(v .. " has been initialized (" .. path .. ")")
	end)

	if not success then
		E.out("failed to load module " .. v .. " (" .. path .. ")", Color3.new(1, 0, 0))
	end

	return success
end

E.unloadModule = function(module)
	module.unload()
	for i,v in next, E.modules do
		if v.file == module.file then
			table.remove(E.modules, i)
			for x = #loaded, 1, -1 do
				if loaded[x] == v.file then
					table.remove(loaded, x)
				end
			end
			break
		end
	end
end

E.reloadModule = function(module)
	E.debug("reloading module " .. module.name .. " (" .. module.file .. ")", Color3.new(1, 0.6, 0))
	E.unloadModule(module)
	E.loadModule(module.file)
end

for i,v in next, MODULE_FILES do
	E.loadModule(v)
end
E.out("successfully loaded " .. #loaded .. " modules", Color3.new(0, 0.8, 0))