# empyrean

empyrean is a modular in-game console designed for Elysian. Most parts of it are modules, and can thereby be modified, including the UI and input modules.

## Using

Download the repository and place `scripts/empyrean.lua` in your `scripts/` directory in Elysian, and likewise with `workspace/empyrean/` scripts. Freely modify the `USER PREFERENCE` section in `empyrean.lua` to your liking.

Depending on what UI you use, entering commands and viewing the console will be different. However, all commands work under a common syntax of `command arg1 arg2 "arg3 with spaces"`, where you can include spaces in quoted arguments. You can run multiple commands using the empyrean command parser using &&, so you could easily do `command1 && command2 arg1`.

### Default UI

The default UI (`workspace/empyrean/default_ui.lua`) uses backslash as input. It can be changed in the `USER PREFERENCE` section, the variable `CONSOLE_KEY`. If you choose not to load this module in your `empyrean.lua`, you can write your own UI module, so long as it comes before most other additional modules.

![Preview image of default UI](https://i.imgur.com/qCp0G85.jpg)

## Writing modules

To write a module, create a new file in your `workspace/empyrean` directory. Modules are formatted in snake_case. Do not create any directories in the empyrean directory. It is a good idea to look at the example module, as it exhibits some good practices when writing modules.

All global empyrean methods are under a `E` table. That said, you need to create empyrean-based objects using it.

To create a new module, use `E.createModule(file, name, description)`. It returns a module object. `file` is the name of the file, minus the `.lua`.

To create a new command, use `E.createCommand(aliases, name, description, func)`. It returns a command object. Aliases is an array, and func is a function that passes arguments `args`, an array of strings, and `raw`, the original group of arguments unsplit.

Finally, to finish your module, you need to bind the commands to the module and bind the module to empyrean. Every module has an `init` method. In it, (no pun intended) you need to bind your commands, like `module.bindCommand(command)`, and do any other things that must be ran when you want the module to initialize.

In order for your module to work properly with `modules`'s `reload` command, you may need to implement a `module.unload` function. By default it does nothing. Disconnect any events you've connected, destroy any instances, etc.

You can then at the end of the file bind the module to empyrean with `E.bindModule(module)`.

### Other useful methods

`E.out(text, [color])` writes some text onto the console.

`E.players.match(text)` will search for a **single** player in the game, like their name, `me`, `others`, or `all`.
`E.players.matchSome(text)` will search for multiple players in the game, like above.

`E.parseCommand(text)` will parse a command as if it was put into the console.

## Contributing

You can freely contribute to this project, although not much more is necessary to the base project. You should focus on creating modules for empyrean, in which case, you could create a PR and add them to this `workspace/empyrean`, or myself or another could create an `awesome_empyrean` for excessive modules.