--------------------------------
-- Teleportation and Movement --
---- voximity ------------------

local player = game:GetService("Players").LocalPlayer

local emodule = E.createModule("movement", "Teleportation and Movement", "Adds various movement commands and teleportation commands.")

local tptocommand = E.createCommand({"to", "tpto"}, "Teleport To", "Teleports to someone.", function(args)
	local target = E.players.match(args[1])
	player.Character.HumanoidRootPart.CFrame = target.Character.HumanoidRootPart.CFrame
end)

local tpherecommand = E.createCommand({"here", "tphere"}, "Teleport Here", "Teleports someone to you. Only works if FilteringEnabled is disabled.", function(args)
	if game:GetService("Workspace").FilteringEnabled then
		E.out("filteringenabled is enabled", Color3.new(1, 0, 0))
		return
	end
	local target = E.players.matchSome(args[1])
	for i,v in next, target do
		v.Character.HumanoidRootPart.CFrame = player.Character.HumanoidRootPart.CFrame
	end
end)

local tpcommand = E.createCommand({"tp", "teleport"}, "Teleport", "Teleports someone to someone else. Only works if FilteringEnabled is disabled, or if the first player is yourself.", function(args)
	local targets = E.players.matchSome(args[1])
	local target = E.players.match(args[2])
	if not (#targets == 1 and targets[1] == player) and game:GetService("Workspace").FilteringEnabled then
		E.out("filteringenabled is enabled", Color3.new(1, 0, 0))
		return
	end
	for i,v in next, targets do
		v.Character.HumanoidRootPart.CFrame = target.Character.HumanoidRootPart.CFrame
	end
end)

local forwardcommand = E.createCommand({"f", "fwd", "forward"}, "Forward", "Teleports yourself forward a specific amount of studs, or 5 if unspecified.", function(args)
	local distance = tonumber(args[1]) or 5
	player.Character.HumanoidRootPart.CFrame = player.Character.HumanoidRootPart.CFrame * CFrame.new(0, 0, -distance)
end)

local upcommand = E.createCommand({"u", "up"}, "Up", "Teleports yourself upward a specific amount of studs, or 20 if unspecified.", function(args)
	local distance = tonumber(args[1]) or 20
	player.Character.HumanoidRootPart.CFrame = player.Character.HumanoidRootPart.CFrame * CFrame.new(0, distance, 0)
end)

local resetcommand = E.createCommand({"reset"}, "Reset", "Resets your character by killing it. Will attempt to LoadCharacter if load is specified.", function(args)
	local t = args[1]
	if t == "load" then
		player:LoadCharacter()
	else
		player.Character:BreakJoints()
	end
end)

emodule.init = function()
	emodule.bindCommand(tptocommand)
	emodule.bindCommand(tpherecommand)
	emodule.bindCommand(tpcommand)

	emodule.bindCommand(forwardcommand)
	emodule.bindCommand(upcommand)

	emodule.bindCommand(resetcommand)
end

E.bindModule(emodule)