--------------------
-- Module Manager --
---- voximity ------

local emodule = E.createModule("modules", "Module Manager", "Adds command interfaces for loading, reloading, and unloading modules in realtime.")

local function longest(array, check)
	local length = 0
	local value = ""
	for i,v in next, array do
		local checked = check(v)
		if #checked > length then
			length = #checked
			value = checked
		end
	end
	return value, length
end

local modulescommand = E.createCommand({"modules", "moduleslist"}, "Module List", "Shows a list of loaded modules.", function(args)
	E.out("   " .. #E.modules .. " are active", Color3.new(0, 0.8, 0))
	local longest, longestlength = longest(E.modules, function(m) return m.name end)
	for i,v in next, E.modules do
		E.out(i .. ") " .. v.name .. ": " .. string.rep(" ", longestlength - #v.name) .. v.description)
	end
	E.out("")
end)

local loadcommand = E.createCommand({"load", "loadmodule"}, "Load Module", "Loads a module by its file name, like in MODULE_FILES.", function(args)
	local file = args[1]
	E.loadModule(file)
end)

local reloadcommand = E.createCommand({"reload", "reloadmodule"}, "Reload Module", "Reloads a module by its file name, like in MODULE_FILES.", function(args)
	local file = args[1]
	local module = -1
	for i,v in next, E.modules do
		if v.file:lower() == file:lower() then
			module = v
		end
	end
	if module == -1 then
		E.out("no module found", Color3.new(1, 0, 0))
		return
	end
	E.reloadModule(module)
end)

local unloadcommand = E.createCommand({"unload", "unloadmodule"}, "Unload Module", "Unload a module by its file name, like in MODULE_FILES.", function(args)
	local file = args[1]
	local module = -1
	for i,v in next, E.modules do
		if v.file:lower() == file:lower() then
			module = v
		end
	end
	if module == -1 then
		E.out("no module found", Color3.new(1, 0, 0))
		return
	end
	E.unloadModule(module)
	E.out("unloaded module " .. module.name .. " (" .. module.file .. ")")
end)

emodule.init = function()
	emodule.bindCommand(modulescommand)

	emodule.bindCommand(loadcommand)
	emodule.bindCommand(reloadcommand)
	emodule.bindCommand(unloadcommand)
end

E.bindModule(emodule)