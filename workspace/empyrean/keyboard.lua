local uis = game:GetService("UserInputService")

local emodule = E.createModule("keyboard", "Keyboard", "Adds input-related commands.")
emodule.binds = {}
local listenerfunction = function(input)
	if uis:GetFocusedTextBox() ~= nil then return end
	for i,v in next, emodule.binds do
		if input.KeyCode.Name:lower() == v.key then
			E.parseCommand(v.command)
		end
	end
end

local bindcommand = E.createCommand({"bind", "b"}, "Bind", "Binds a command to a key.", function(args, raw)
	local key = args[1]:lower()
	local split = -1
	for i = 1, #raw do
		if raw:sub(i, i) == " " then
			split = i
			break
		end
	end
	local command = raw:sub(split + 1)
	table.insert(emodule.binds, {key = key, command = command})
	E.out("bound " .. key .. " to " .. command)
end)
local unbindcommand = E.createCommand({"unbind", "unb"}, "Unbind", "Unbinds a command from a key, if a bind exists.", function(args)
	local key = args[1]:lower()
	local successes = 0
	for i = #emodule.binds, 1, -1 do
		if emodule.binds[i].key == key then
			successes = successes + 1
			table.remove(emodule.binds, i)
		end
	end
	if successes == 0 then
		E.out("no binds to unbind")
	else
		E.out("unbound " .. successes .. " instance(s) of " .. key)
	end
end)
local bindscommand = E.createCommand({"binds"}, "Binds", "Lists all binds active.", function(args)
	if #emodule.binds == 0 then
		E.out("no binds are active")
	else
		for i,v in next, emodule.binds do
			E.out("key " .. v.key:lower() .. " is bound to " .. v.command)
		end
	end
end)

emodule.init = function()
	emodule.listener = uis.InputBegan:connect(listenerfunction)

	-- haha this is totally meta
	emodule.bindCommand(bindcommand)
	emodule.bindCommand(unbindcommand)
	emodule.bindCommand(bindscommand)
end

emodule.unload = function()
	emodule.listener:disconnect()
end

E.bindModule(emodule)