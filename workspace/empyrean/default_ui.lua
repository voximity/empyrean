-----------------------------
-- Default UI for Empyrean --
---- voximity ---------------

local emodule = E.createModule("default_ui", "Default UI", "The default UI for empyrean.")

local player = game:GetService("Players").LocalPlayer
local coregui = game:GetService("CoreGui")

local clearcommand = E.createCommand({"clear", "clean", "cls", "clr"}, "Clear", "Clears the UI.", function(args)
	E.output = {}
	renderlines()
end)

E.out = function(text, color)
	table.insert(E.output, {text = text, color = color})
	renderlines()
end

function renderline(output)
	local text = Instance.new("TextLabel")
	text.Text = output.text
	text.TextColor3 = output.color or Color3.new(1, 1, 1)
	text.Font = "Code"
	text.TextSize = 14
	text.Size = UDim2.new(0, 0, 0, 14)
	text.BackgroundTransparency = 1
	text.TextXAlignment = "Left"
	return text
end

function renderlines(guiornone)
	(guiornone or gui()).lines:ClearAllChildren()
	for i,v in next, E.output do
		local label = renderline(v)
		label.Position = UDim2.new(0, 0, 0, 14 * (i - 1))
		label.Parent = (guiornone or gui()).lines
	end
end

function gui()
	if coregui:FindFirstChild("empyrean_ui") then
		return coregui.empyrean_ui
	end
	local ui = Instance.new("ScreenGui")
	ui.Name = "empyrean_ui"

	local linesfolder = Instance.new("Folder")
	linesfolder.Name = "lines"
	linesfolder.Parent = ui

	renderlines(ui)

	ui.Parent = coregui
	return ui
end
function showinput()
	if gui():FindFirstChild("box") ~= nil then
		return
	end
	local box = Instance.new("TextBox")
	box.Name = "box"
	box.Text = ""
	box.ClearTextOnFocus = true
	box.BackgroundTransparency = 0.5
	box.BackgroundColor3 = Color3.new(0, 0, 0)
	box.TextColor3 = Color3.new(1, 1, 1)
	box.TextTransparency = 0
	box.TextXAlignment = "Left"
	box.Font = "Code"
	box.TextSize = 18
	box.BorderSizePixel = 0
	box.Size = UDim2.new(1, 0, 0, 20)
	box.Position = UDim2.new(0, 0, 1, 0)
	box.Parent = gui()
	box.FocusLost:connect(function(enter)
		if enter then
			local text = box.Text
			if text ~= "" and text ~= " " then
				E.out("> " .. text, Color3.new(0, 0.7, 1))
				spawn(function() E.parseCommand(text) end)
			end
		end
		box:Destroy()
	end)
	box:CaptureFocus()
	box:TweenPosition(UDim2.new(0, 0, 1, -20), "Out", "Quint", 0.5)
end

emodule.init = function()
	emodule.listener = game:GetService("UserInputService").InputBegan:connect(function(input)
		if game:GetService("UserInputService"):GetFocusedTextBox() ~= nil then return end
		if input.KeyCode == CONSOLE_KEY then
			showinput()
		end
	end)

	emodule.bindCommand(clearcommand)
end

emodule.unload = function()
	emodule.listener:disconnect()
	gui():Destroy()
end

E.bindModule(emodule)