---------------------
-- Empyrean Manual --
---- voximity -------

local emodule = E.createModule("man", "Manual", "Empyrean manual adding help commands.")

local function longest(array, check)
	local length = 0
	local value = ""
	for i,v in next, array do
		local checked = check(v)
		if #checked > length then
			length = #checked
			value = checked
		end
	end
	return value, length
end

local helpcommand = E.createCommand({"help", "cmds", "?"}, "Help", "Shows a basic list of every command.", function(args)
	-- i already see a flaw with this. if for some reason so many commands are created that there's a necessity for pagination, i'll do it.

	local function aliases(c) return "[" .. table.concat(c.aliases, ", ") .. "]" end

	local _, longestname = longest(E.getCommands(), function(c) return c.name end)
	local _, longestaliases = longest(E.getCommands(), aliases)
	for i,v in next, E.modules do
		E.out(v.name .. " has " .. #v.commands .. " command" .. (#v.commands == 1 and "" or "s"), Color3.new(0, 0.8, 0))
		for a,b in next, v.commands do
			local al = aliases(b)
			E.out(al .. " " .. string.rep(" ", longestaliases - #al) .. b.name .. ": " .. string.rep(" ", longestname - #b.name) .. b.description)
		end
		E.out("")
	end
end)
local reloadcommand = E.createCommand({"reload"}, "Reload Module", "Reload a module.")

emodule.init = function()
	emodule.bindCommand(helpcommand)
end

E.bindModule(emodule)