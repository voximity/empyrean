---------------------
-- Empyrean Aimbot --
---- voximity -------

local emodule = E.createModule("aim", "Aimbot", "Adds aimbotting features, as well as a few wallhack features.")

emodule.aim = {}
emodule.aim.target = nil
emodule.aim.enabled = false
emodule.aim.torsotarget = false
emodule.aim.whitelist = {}

local player = game:GetService("Players").LocalPlayer
local camera = game:GetService("Workspace").CurrentCamera

local function shouldtarget(a)
	if player.Team == nil then return true end
	if player.Team ~= a.Team then return true end
	return false
end

local function chams()
	if game:GetService("CoreGui"):FindFirstChild("e_chams") ~= nil then
		return game:GetService("CoreGui").e_chams
	end
	local c = Instance.new("Folder")
	c.Name = "e_chams"
	c.Parent = game:GetService("CoreGui")
	return c
end

local function champlayer(target)
	if target.Character == nil then
		return false
	end

	local character = target.Character
	if chams():FindFirstChild(target.Name) ~= nil then
		chams()[target.Name]:Destroy()
	end

	local folder = Instance.new("Folder")
	folder.Name = target.Name
	
	for i,v in next, character:GetChildren() do
		if v:IsA("BasePart") then
			local bha = Instance.new("BoxHandleAdornment")
			bha.Name = v.Name
			bha.Size = v.Size * 1.05
			bha.Adornee = v
			bha.AlwaysOnTop = true
			bha.Color3 = target.Team.TeamColor.Color
			bha.ZIndex = 10
			bha.Parent = folder
		end
	end

	folder.Parent = chams()
	return #folder:GetChildren() > 0
end

local chamscommand = E.createCommand({"chams", "wallhack"}, "Player Chams", "Creates or refreshes chams for every other player in the server.", function(args)
	chams():ClearAllChildren()
	local successes = 0
	for i,v in next, game:GetService("Players"):GetPlayers() do
		if v ~= player then
			if champlayer(v) then
				successes = successes + 1
			end
		end
	end
	E.out("created chams for " .. successes .. " player(s)", Color3.new(0, 0.8, 0))
end)
local disablechamscommand = E.createCommand({"disablechams", "nochams"}, "Disable Player Chams", "Deletes all existing player chams.", function(args)
	chams():ClearAllChildren()
	E.out("removed all chams", Color3.new(1, 0, 0))
end)
local prefercommand = E.createCommand({"aimprefer"}, "Prefer Aim", "Choose between torso or head to aim at.", function(args)
	if args[1] == "torso" then
		emodule.aim.torsotarget = true
		E.out("torso aim set")
	elseif args[1] == "head" then
		emodule.aim.torsotarget = false
		E.out("head aim set")
	else
		E.out(args[1] .. " is not \"torso\" or \"head\"", Color3.new(1, 0, 0))
	end
end)
local togglecommand = E.createCommand({"aimtoggle", "aim"}, "Toggle Aim", "Toggles aimbot.", function(args)
	emodule.aim.enabled = not emodule.aim.enabled
	if emodule.aim.enabled then
		E.out("aimbot enabled", Color3.new(0, 0.8, 0))
	else
		E.out("aimbot disabled", Color3.new(1, 0, 0))
	end
end)
local aimatcommand = E.createCommand({"aimat", "aimto"}, "Aim At", "Aims at a specific player, or a near one. Specify \"visible\" to get a near visible player.", function(args)
	local target = args[1]
	local playerTarget = -1

	if target == "nearest" or target == "" or target == nil then
		local goodtargets = {}
		for i,v in next, game:GetService("Players"):GetPlayers() do
			if v.UserId ~= player.UserId and shouldtarget(v) and v.Character ~= nil and v.Character:FindFirstChild("Head") ~= nil and v.Character:FindFirstChild("Humanoid") and v.Character.Humanoid.Health ~= 0 then
				table.insert(goodtargets, v)
			end
		end
		local nearestdist = -1
		for i,v in next, goodtargets do
			local dist = (camera.CoordinateFrame.p - v.Character.Head.Position).magnitude
			if dist < nearestdist or nearestdist == -1 then
				playerTarget = v
				nearestdist = dist
			end
		end
	elseif target == "visible" then
		local goodtargets = {}
		for i,v in next, game:GetService("Players"):GetPlayers() do
			if v.UserId ~= player.UserId and shouldtarget(v) and v.Character ~= nil and v.Character:FindFirstChild("Head") ~= nil and v.Character:FindFirstChild("Humanoid") and v.Character.Humanoid.Health ~= 0 then
				table.insert(goodtargets, v)
			end
		end
		local nearestdist = -1
		for i,v in next, goodtargets do
			local dist = (camera.CoordinateFrame.p - v.Character.Head.Position).magnitude
			local ray = Ray.new(camera.CoordinateFrame.p, (v.Character.Head.Position - camera.CoordinateFrame.p).unit * dist)
			local part = game:GetService("Workspace"):FindPartOnRayWithIgnoreList(ray, {player.Character, v.Character})
			if (dist < nearestdist or nearestdist == -1) and part == nil then
				playerTarget = v
				nearestdist = dist
			end
		end
	else
		playerTarget = E.players.match(target)
	end

	if playerTarget == -1 then
		E.out("no player selected", Color3.new(1, 0, 0))
		return
	elseif playerTarget.Character == nil then
		E.out("player has no character", Color3.new(1, 0, 0))
		return
	elseif (not emodule.aim.torsotarget and playerTarget.Character:FindFirstChild("Head") == nil) or (emodule.aim.torsotarget and playerTarget.Character:FindFirstChild("HumanoidRootPart") == nil) then
		E.out("player has no head and/or torso", Color3.new(1, 0, 0))
		return
	end

	emodule.aim.target = not emodule.aim.torsotarget and playerTarget.Character.Head or playerTarget.Character.HumanoidRootPart
	E.out("aiming at " .. playerTarget.Name)

end)

emodule.aim.update = function()
	if emodule.aim.enabled and emodule.aim.target ~= nil and emodule.aim.target.Parent ~= nil then
		camera.CoordinateFrame = CFrame.new(camera.CoordinateFrame.p, emodule.aim.target.Position)
	end
end

emodule.init = function()
	emodule.listener = game:GetService("RunService").Heartbeat:connect(emodule.aim.update)

	emodule.bindCommand(chamscommand)
	emodule.bindCommand(disablechamscommand)

	emodule.bindCommand(prefercommand)
	emodule.bindCommand(togglecommand)
	emodule.bindCommand(aimatcommand)
end

emodule.unload = function()
	emodule.listener:disconnect()
end

E.bindModule(emodule)