--------------------------------
--------------------------------
--     empyrean/example.lua   --
--          voximity          --
--------------------------------
--------------------------------


-- Create a new module object using empyrean. It is not read by empyrean until it is binded.
local emodule = E.createModule("example", "Example Module", "An example module for empyrean.")

-- Create a new command object. It is also not read until binded.
local examplecommand = E.createCommand({"example"}, "Example Command", "Example command for example module.", function(args)
	if #args == 0 then
		E.out("you used the example command. cool!")
	else
		E.out("you used the example command. your args were: " .. table.concat(args, ", "))
	end
end)

-- This function will be ran as soon as the module is binded.
emodule.init = function()
	E.out("initialized example empyrean module :D")

	-- Bind any commands you have created to the module.
	emodule.bindCommand(examplecommand)
end

-- This function will be called when the module requests an unload or reload. Dispose of any events or instances you don't want carried over here.
emodule.unload = function()

end

-- Bind the module, telling empyrean to initialize and activate the module.
E.bindModule(emodule)